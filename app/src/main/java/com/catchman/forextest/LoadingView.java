package com.catchman.forextest;

/**
 * @author Artur Vasilov
 */
public interface LoadingView {

    void showLoadingIndicator(String message);

    void hideLoadingIndicator();

}
