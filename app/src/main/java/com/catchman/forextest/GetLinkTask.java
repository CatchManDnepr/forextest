package com.catchman.forextest;

import android.os.AsyncTask;
import android.util.Log;

import com.catchman.forextest.main.SiteListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class GetLinkTask extends AsyncTask<String, String, String> {

    private SiteListener siteListener;

    public GetLinkTask(SiteListener siteListener) {
        this.siteListener = siteListener;
    }

    protected String doInBackground(String ... link) {

        try {
            siteListener.setSite(Jsoup.connect(link[0]).get());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
