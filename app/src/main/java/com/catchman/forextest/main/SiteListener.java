package com.catchman.forextest.main;

import org.jsoup.nodes.Document;

public interface SiteListener {

    void setSite(Document document);
}
