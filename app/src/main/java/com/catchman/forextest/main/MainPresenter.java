package com.catchman.forextest.main;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;

    public MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void parsingSite(Document doc) {

        Element mainPanel = doc.body().children().get(2);
        Element leftSide = mainPanel.children().get(0);
        Element table = leftSide.children().get(2);
        Element tableSource = table.getElementsByClass("swap-vr").get(0).
                getElementsByClass("body-t").get(0);
        Elements tableItem = tableSource.children();
        for (Element element: tableSource.getElementsByClass("o-tr-line active-pr-alert")) {
            Log.e("???", element.className());
        }
//        Element mainContentFrame = mainPanel.getElementById("mainContentFrame");

    }
}
