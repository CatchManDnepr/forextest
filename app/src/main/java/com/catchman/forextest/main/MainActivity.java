package com.catchman.forextest.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;

import com.catchman.forextest.GetLinkTask;
import com.catchman.forextest.base.BaseActivity;
import com.catchman.forextest.R;
import com.catchman.forextest.databinding.ActivityMainBinding;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class MainActivity extends BaseActivity implements MainContract.View, SiteListener{

    private ActivityMainBinding binding;
    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        presenter = new MainPresenter(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("!!!", "onStart: ");
        new GetLinkTask(this).execute("https://trade.arotrade.com/cfd");
    }

    @Override
    public void setSite(Document document) {
        presenter.parsingSite(document);
    }

}
