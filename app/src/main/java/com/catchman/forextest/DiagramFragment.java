package com.catchman.forextest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.catchman.forextest.base.BaseFragment;
import com.catchman.forextest.databinding.FragmentDiagramBinding;

public class DiagramFragment extends BaseFragment<FragmentDiagramBinding> {


    @Override
    protected void setUpViews() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_diagram, container, false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diagram;
    }
}
