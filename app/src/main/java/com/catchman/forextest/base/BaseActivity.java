package com.catchman.forextest.base;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.catchman.forextest.LoadingDialog;
import com.catchman.forextest.LoadingView;
import com.catchman.forextest.R;

import java.io.IOException;


/**
 * Created by ujujzk on 10.08.2017
 * Softensy Digital Studio
 * softensiteam@gmail.com
 */

public class BaseActivity extends AppCompatActivity {


    private LoadingView mLoadingView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());
    }

    public void showProgress(boolean show) {
        showProgress(getString(R.string.dialog_loading), show);
    }

    public void showProgress(String message, boolean show) {

        if (show) {
            mLoadingView.showLoadingIndicator(message);
        } else {
            mLoadingView.hideLoadingIndicator();
        }

    }


    public void notOnline(){
        Toast.makeText(this, R.string.not_connection_to_internet, Toast.LENGTH_SHORT).show();
    }


}
